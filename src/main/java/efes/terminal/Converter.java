package main.java.efes.terminal;

import java.math.BigDecimal;

@FunctionalInterface
public interface Converter<T> {

    BigDecimal convert(T from);

}
