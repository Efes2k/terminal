package main.java.efes.terminal;

import org.apache.commons.lang3.tuple.Pair;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.BiConsumer;
import java.util.function.Function;
import java.util.stream.Collectors;

public class TerminalService {

    public static <T> Map<T, Long> calculateBanknotes(BigDecimal sum, Map<T, Long> availableNumbers, Converter<T> converter) {

        Map<T, Long> result = new HashMap<>();

        List<Pair<T, BigDecimal>> sortedNumbers = availableNumbers
                .entrySet()
                .stream()
                .map(i -> Pair.of(i.getKey(), converter.convert(i.getKey())))
                .sorted((l1,l2) -> l1.getRight().compareTo(l2.getRight()))
                .collect(Collectors.toList());

        class Wrapper<T> {
            private T function;
        }

        Wrapper<Function<BigDecimal, BiConsumer<Integer, Map<T, Long>>>> recursion = new Wrapper<>();

        recursion.function = money -> (numberOfNominal, buffer) -> {
            if (money.compareTo(BigDecimal.ZERO) == 0) {
                result.putAll(buffer);
                return;
            }
            if (money.compareTo(BigDecimal.ZERO) < 0 || numberOfNominal < 0)
                return;

            final Pair<T, BigDecimal> currentItem = sortedNumbers.get(numberOfNominal);
            final BigDecimal currentNumber = currentItem.getRight();
            final Long availableCount = availableNumbers.get(currentNumber);
            final Long necessaryCount = money.divideToIntegralValue(currentNumber).longValue();

            BigDecimal takenSum = BigDecimal.ZERO;
            final Long takenCount = availableCount - necessaryCount > 0 ? necessaryCount : availableCount;
            if(takenCount.compareTo(0L) > 0) {
                takenSum = currentNumber.multiply(BigDecimal.valueOf(takenCount));

            }

            buffer.put(currentItem.getLeft(), takenCount);
            recursion.function.apply(money.subtract(takenSum)).accept(numberOfNominal - 1, new HashMap<>(buffer));

        };

        recursion.function.apply(sum).accept(sortedNumbers.size() - 1, new HashMap<>());

        return result;
    }

}
