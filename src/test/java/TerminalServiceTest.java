package test.java;


import main.java.efes.terminal.TerminalService;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

public class TerminalServiceTest {



    @Test
    public void simpleTest(){

        Map<BigDecimal, Long> numbers = new HashMap<>();
        numbers.put(new BigDecimal(1), 31L);
        numbers.put(new BigDecimal(10), 10L);
        numbers.put(new BigDecimal(5), 45L);
        numbers.put(new BigDecimal(50), 2L);
        numbers.put(new BigDecimal(20), 9L);
        numbers.put(new BigDecimal(2), 7L);

        final BigDecimal necessarySum = new BigDecimal(123);
        Map<BigDecimal, Long> result = TerminalService.calculateBanknotes(necessarySum, numbers, from -> from);

        final BigDecimal resultSum = result.entrySet()
                .stream()
                .map(n -> n.getKey().multiply(BigDecimal.valueOf(n.getValue())))
                .reduce(BigDecimal.ZERO, BigDecimal::add);

        Assert.assertEquals(necessarySum, resultSum);
        System.out.println("sum = " + necessarySum + " splitted to " + result);

    }
}
